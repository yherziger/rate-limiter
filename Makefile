#!make

PY_BIN = python3

.PHONY: install
install:
	$(PY_BIN) -m pip install -r dev-requirements.txt

.PHONY: test
test:
	$(PY_BIN) -m pytest ./tests
