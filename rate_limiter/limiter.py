import logging
from functools import wraps

from rate_limiter.stores import BaseStore, InMemoryStore

LOGGER = logging.getLogger(__name__)


class RateLimitExceeded(Exception):
    status_code = 429

    def __str__(self):
        return "HTTP 429: Rate Limit Exceeded"


class Request:
    user_id: str
    ip: str

    def __init__(self, user_id: str, ip: str):
        self.user_id = user_id
        self.ip = ip


class RequestLimiter:
    """
    RequestLimiter is a class that can be instantiated to decorate
    request endpoints. It is instantiated with a store argument that
    necessarily implements <stores.BaseStore>.
    """

    def __init__(self, store: BaseStore):
        self.store = store

    def rate_limit(self, x: int = 1, y: int = 1, type: str = "user"):
        """
        A decorator function to control the request rate for a web handler.

        :param x:           Max # of requests in y seconds
        :param y:           Time window (in seconds) to perform a maximum of x successful requests
        :param type:        Either "user" or "ip"
        :return:            Decorated handler function
        """

        def _decorator(func):

            @wraps(func)
            def _wrapper(*args, **kwargs):
                # Get the request object, assuming it's the first positional argument of the handler:
                (request,) = args
                # Infer key value from key type:
                key = request.user_id if type == "user" else request.ip
                if not self.store.agent_exists(key=key, time_window_sec=y):
                    # If the agent doesn't have a rate limit record, create one and initialize a request bucket:
                    self.store.create_agent(key=key, max_requests=x, time_window_sec=y)

                bucket_value = self.store.get_bucket_value(key)
                if bucket_value and bucket_value > 0:
                    self.store.decrement_agent_bucket(key=key)
                    return func(*args, **kwargs)
                if not self.store.is_agent_exceeded(key=key, max_requests=x, time_window_sec=y):
                    LOGGER.info("User [%s = %s] hit their request limit", type, key)
                    self.store.mark_agent_exceeded(key=key)
                raise RateLimitExceeded

            return _wrapper

        return _decorator
