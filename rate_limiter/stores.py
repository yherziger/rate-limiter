import time
import logging
from typing import Any, Dict

LOGGER = logging.getLogger(__name__)


class BaseStore:
    """
    BaseStore is a base class that different agent store implementations should
    extend (e.g., Redis, Memcached, RDMBS, or any other persistence layer.)
    Extend this class to implement any type of caching layer.
    """

    def get_agent(self, key: str) -> Any:
        raise NotImplementedError

    def agent_exists(self, key, time_window_sec: int) -> bool:
        raise NotImplementedError

    def create_agent(self, key, max_requests: int, time_window_sec: int):
        raise NotImplementedError

    def reset_agent(self, key: str) -> None:
        raise NotImplementedError

    def decrement_agent_bucket(self, key: str, decrement: int = 1) -> None:
        raise NotImplementedError

    def mark_agent_exceeded(self, key: str):
        raise NotImplementedError

    def is_agent_exceeded(self, key: str, max_requests: int, time_window_sec: int) -> bool:
        raise NotImplementedError

    def get_bucket_value(self, key) -> int:
        raise NotImplementedError


class InMemoryStore(BaseStore):
    """
    Memory store should only be used for testing purposes.  Do not use in production since every
    instance of the service will have its own state.  Without session affinity, you're in risk of
    inconsistent request tracking.
    """
    store: Dict = {}

    def __init__(self):
        LOGGER.warning("You are using an in-memory store - do not use in production")

    def get_agent(self, key: str) -> Any:
        return self.store.get(key)

    def agent_exists(self, key, time_window_sec: int) -> bool:
        agent = self.get_agent(key)
        unix_now_ms = time.time_ns() // 1_000_000
        if agent and agent["expires"] >= unix_now_ms:
            return True
        return False

    def create_agent(self, key, max_requests: int, time_window_sec: int) -> None:
        unix_now_ms = time.time_ns() // 1_000_000
        self.store[key] = {
            "expires": unix_now_ms + (time_window_sec * 1000),
            "bucket": max_requests,
        }

    def reset_agent(self, key: str) -> None:
        if key in self.store:
            del self.store[key]

    def decrement_agent_bucket(self, key: str, decrement: int = 1) -> None:
        if key in self.store:
            self.store[key]["bucket"] -= decrement

    def mark_agent_exceeded(self, key: str):
        if key in self.store:
            self.store[key]["rate_limit_hit"] = True

    def is_agent_exceeded(self, key: str, max_requests: int, time_window_sec: int) -> bool:
        return self.get_agent(key).get("rate_limit_hit") is not None

    def get_bucket_value(self, key) -> int:
        return self.store.get(key, {}).get("bucket", 0)
