# Rate Limiter Assignment

## Comments

Thanks for taking the time to review my assignment. Please find a few remarks on the assignment below.

* I've chosen to implement the decorator inside a class I named `RequestLimiter`. The reason I opted for this approach
  is that ideally, I'd want every endpoint/handler decorated with this function to use the same store. Instantiating
  a class for this made sense as a quick & dirty solution to achieve that goal.

* I've chosen to have the `RequestLimiter` class accept an instance of a base state class. This class is not implemented.
  I've done that so that when a state store is incorporated, it can easily extend this base class, be it any type of
  state. Examples for clients that could theoretically implement the `BaseStore` class:

    * A caching engine like Redis, Memcached, etc.
    * An RDBMS
    * A document/NoSQL DB
    * etc.

  Please note that the unit tests (see below) are leveraging an in-memory example implementation of the `BaseStore`
  class.  This class should not be used in production.

* **Limitation:** my implementation is time-bucketed. In practice, rate limiting can be a lot more complex.
  "Time-bucketed" essentially means that I'm using a "bucket" of allowed requests (`x`)
  over a set time window (`y`).  Other algorithms, such as [_Leaky Bucket_](https://en.wikipedia.org/wiki/Leaky_bucket)
  and [_Generic Cell Rate_](https://en.wikipedia.org/wiki/Generic_cell_rate_algorithm) could be incorporated for a more
  sophisticated mechanism.

* **Limitation:** my code is blocking. It's relying on the web server using it to either be wrapped in WSGI/ASGI middleware
  to make it non-blocking.

* **Limitation:** there's no standard model for the "agent" (or "user") object. The reason I left it this way is that in
  practice, this object can be an ORM model (e.g., Django ORM or SQLAlchemy) or a generic `dict` in other cases.

## Running the Tests

Requirements:

* Python 3

1. To install the single dependency the tests are using (`pytest`) use the following command:

   ```shell
   make install
   ```

   If you're not working in a Linux/Unix environment with GNU Make available, run the following command:

   ```shell
   python -m pip install -r dev-requirements.txt
   ```

2. Run the tests:

   ```shell
   make test
   ```

   If you're not working in a Linux/Unix environment with GNU Make available, run the following command:

   ```shell
   python -m pytest ./tests
   ```
