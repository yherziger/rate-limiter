from time import sleep

from rate_limiter import RequestLimiter, InMemoryStore, RateLimitExceeded, Request


class TestLimiter:

    @staticmethod
    def test_within_allowance():
        limiter = RequestLimiter(store=InMemoryStore())

        # 10 requests per second
        @limiter.rate_limit(x=10, y=1, type="user")
        def get_music(request):
            return f"Hello, {str(request)}"

        success = True
        try:
            # Make 9 requests
            for i in range(9):
                get_music(Request("john.doe@company.com", "127.0.0.1"))
        except RateLimitExceeded:
            success = False

        assert success, "At least one request was rate-limited but none were supposed to be"

    @staticmethod
    def test_allowance_breach():
        limiter = RequestLimiter(store=InMemoryStore())

        # 10 requests per second
        @limiter.rate_limit(x=10, y=1, type="ip")
        def get_music(request):
            return f"Hello, {str(request)}"

        def make_req():
            try:
                get_music(Request("john.doe@company.com", "127.0.0.1"))
                return True
            except RateLimitExceeded:
                return False

        successful_requests = 0
        blocked_requests = 0

        # Make 10 requests
        for i in range(10):
            success = make_req()
            if success:
                successful_requests += 1
            else:
                blocked_requests += 1

        # Sleep 0.5 sec.
        sleep(0.5)
        # Make 5 requests, should be blocked
        for i in range(5, 10):
            success = make_req()
            if success:
                successful_requests += 1
            else:
                blocked_requests += 1
        # Sleep 0.5 sec.
        sleep(0.5)
        # Make 10 requests, should succeed
        for i in range(10, 15):
            success = make_req()
            if success:
                successful_requests += 1
            else:
                blocked_requests += 1
        assert successful_requests == 15, "Expected 15 requests to succeed"
        assert blocked_requests == 5, "Expected 5 requests to be blocked"
